const TorrentSearchApi = require('torrent-search-api');
const parseTorrent = require('parse-torrent');
const Koa = require('koa');
const app = new Koa();

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

app.use(async ctx => {
    const maxLimit = 10 * ctx.request.query.page;
    //const yggTorrentProvider = await TorrentSearchApi.getProvider('Yggtorrent');
    //yggTorrentProvider.baseUrl = "https://pupflare-cd7xzamp3q-uc.a.run.app/?url=https://www2.yggtorrent.se";
    //yggTorrentProvider.enableCloudFareBypass = false;
    await TorrentSearchApi.enableProvider('YggTorrent', process.env.USERNAME, process.env.PASSWORD);
    let result = await TorrentSearchApi.search(ctx.request.query.query, 'All', maxLimit);
    result = result.slice(maxLimit - 10, maxLimit);
    await asyncForEach(result, async (torrent, index, theArray) => {
        theArray[index].name = theArray[index].title;
        theArray[index].url = theArray[index].desc;
        theArray[index].seeds = theArray[index].seeds.toString();
        theArray[index].peers = theArray[index].peers.toString();
        theArray[index].magnet = await parseTorrent.toMagnetURI(await parseTorrent(await TorrentSearchApi.downloadTorrent(torrent)));
    });
    ctx.body = result;
});

app.listen(3000);